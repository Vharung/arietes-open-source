-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  lun. 07 jan. 2019 à 14:52
-- Version du serveur :  10.1.34-MariaDB
-- Version de PHP :  7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `arietes`
--

-- --------------------------------------------------------

--
-- Structure de la table `claque`
--

CREATE TABLE `claque` (
  `id` int(11) NOT NULL,
  `id_partie` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `idtableau` int(11) NOT NULL,
  `src` varchar(100) NOT NULL,
  `claque` int(1) NOT NULL,
  `x` int(4) NOT NULL,
  `y` int(4) NOT NULL,
  `angle` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `doc`
--

CREATE TABLE `doc` (
  `id` int(11) NOT NULL,
  `idpartie` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `nomdoc` text NOT NULL,
  `contenu` text,
  `contenu_mj` text,
  `visible` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `doc`
--

INSERT INTO `doc` (`id`, `idpartie`, `iduser`, `nomdoc`, `contenu`, `contenu_mj`, `visible`) VALUES
(1, 1, 1, 'Theod 2 ', 'Super fort', 'spycopate', ''),
(2, 1, 2, 'Lettre de salveri', 'tu es un homme mort', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `fiche_block`
--

CREATE TABLE `fiche_block` (
  `id` int(11) NOT NULL,
  `id_table` int(11) NOT NULL,
  `id_block` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nom_comp` varchar(30) NOT NULL,
  `valeur1` text,
  `valeur2` text,
  `valeur3` text,
  `valeur4` text,
  `valeur5` text,
  `valeur6` text,
  `valeur7` text,
  `valeur8` text,
  `valeur9` text,
  `valeur10` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `fiche_block`
--

INSERT INTO `fiche_block` (`id`, `id_table`, `id_block`, `id_user`, `nom_comp`, `valeur1`, `valeur2`, `valeur3`, `valeur4`, `valeur5`, `valeur6`, `valeur7`, `valeur8`, `valeur9`, `valeur10`) VALUES
(1, 1, 4, 1, 'Alauch', 'https://cdnfr1.img.sputniknews.com/images/103083/04/1030830428.jpg', '#d5961c', '#d4e6ff', '3', '2', '2', '1', '2', '4', NULL),
(2, 1, 5, 1, 'PV', '4', '30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 1, 5, 1, 'PSY', '1', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 1, 7, 1, 'Etatjoueur', 'PV', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 1, 14, 1, 'module_text', 'Je suis alauch tanyar', '', '', '', '', '', '', '', '', ''),
(13, 1, 15, 1, 'module_stat', '75', '20', '50', '', '', '', '', '', '', ''),
(14, 1, 16, 1, 'Caratéristique', 'Combat', 'capacité à combattre', '10', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 1, 16, 1, 'Caratéristique', 'Furtivité', 'Capacité à se déplacer discretement', '-10', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 1, 18, 1, 'Armes', 'Haches de bataille', 'Armes préferée', '1', '20', '/r 2d6+1', NULL, NULL, NULL, NULL, NULL),
(17, 1, 19, 1, 'module_enco', '23', '30', '', '', '', '', '', '', '', ''),
(18, 1, 20, 1, 'Inventaire', 'Gourde', 'Pour boire', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL),
(19, 1, 21, 1, 'Dons', 'PHP infuse', 'en progression', '10', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 1, 22, 1, 'Connaissance', 'Savoir infini', 'sais tous sur tout', '5', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 1, 1, 1, 'Point d\'equipe', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 1, 17, 1, 'Connaissance', 'Level', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 1, 17, 1, 'Race', 'php', '', '99', '', '', '', '', '', '', ''),
(28, 1, 9, 1, 'Note MJ', 'Il etait une fois...', 'Dans les Ã©pisodes prÃ©cÃ©dents', 'Ils vont mourrir !\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(35, 1, 4, 3, 'test', NULL, '#4a90e2', '#ffffff', '2', NULL, NULL, NULL, NULL, '3', NULL),
(37, 1, 18, 1, 'module_inv', 'couteau', '30', '2', '1', '', '', '', '', '', ''),
(38, 1, 4, 2, 'ultimate', NULL, '#4a90e2', '#ffffff', NULL, NULL, NULL, NULL, NULL, '6', NULL),
(39, 1, 34, 1, 'module_stat_sec', '65', '10', '5', '30', '10', '50', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `liste`
--

CREATE TABLE `liste` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `template` int(11) NOT NULL,
  `mj` int(11) NOT NULL,
  `color1` varchar(10) DEFAULT NULL,
  `color2` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `liste`
--

INSERT INTO `liste` (`id`, `name`, `template`, `mj`, `color1`, `color2`) VALUES
(1, 'Liber aventure 1', 1, 1, '#778899', '#ffffff'),
(2, 'Liber aventure 2', 3, 2, '#A52A2A', '#FFEBCD'),
(4, 'test2', 3, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `musique`
--

CREATE TABLE `musique` (
  `id` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `dossier` varchar(30) DEFAULT NULL,
  `sousdossier` varchar(30) DEFAULT NULL,
  `nommusique` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `musique`
--

INSERT INTO `musique` (`id`, `iduser`, `dossier`, `sousdossier`, `nommusique`, `url`) VALUES
(1, 1, 'Music', 'epic', 'epic 2h', 'DeXoACwOT1o'),
(2, 1, 'Music', 'epic', 'epic dragon', '51s9fG6hNf4'),
(10, 1, 'Music', 'epic', 'again', 'NoFfgJbkl-o');

-- --------------------------------------------------------

--
-- Structure de la table `playlist`
--

CREATE TABLE `playlist` (
  `id` int(11) NOT NULL,
  `idpartie` int(11) NOT NULL,
  `idmusic` int(11) NOT NULL,
  `lecture_en_cours` int(11) DEFAULT NULL,
  `visible` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `playlist`
--

INSERT INTO `playlist` (`id`, `idpartie`, `idmusic`, `lecture_en_cours`, `visible`) VALUES
(23, 1, 1, 1, 1),
(30, 1, 2, 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `pnj`
--

CREATE TABLE `pnj` (
  `id` int(11) NOT NULL,
  `idpartie` int(11) NOT NULL,
  `nompnj` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `stat` int(11) DEFAULT NULL,
  `histoire` text,
  `notemj` text,
  `visible` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `pnj`
--

INSERT INTO `pnj` (`id`, `idpartie`, `nompnj`, `image`, `stat`, `histoire`, `notemj`, `visible`) VALUES
(1, 1, 'Theod 2', 'img/image.jpg', NULL, 'Super fort', 'spycopate', '2|3'),
(4, 1, 'Nouveau PNJ', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `tableau`
--

CREATE TABLE `tableau` (
  `id` int(11) NOT NULL,
  `id_partie` int(11) NOT NULL,
  `nom tableau` int(11) NOT NULL,
  `visible` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `tables`
--

CREATE TABLE `tables` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_liste` int(11) NOT NULL,
  `visible` int(11) NOT NULL,
  `spetacteur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tables`
--

INSERT INTO `tables` (`id`, `id_user`, `id_liste`, `visible`, `spetacteur`) VALUES
(1, 1, 1, 1, 0),
(4, 1, 4, 0, 0),
(9, 2, 1, 1, 0),
(10, 1, 2, 1, 0),
(11, 3, 1, 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `tchat`
--

CREATE TABLE `tchat` (
  `id_tchat` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_partie` int(11) NOT NULL,
  `id_exp` int(11) NOT NULL,
  `id_dest` int(11) NOT NULL COMMENT '0 si pas mp. sinon id du destinataire',
  `type` int(11) NOT NULL COMMENT '1 message 2 lancer 3 annonce green 4 annonce crimson',
  `text_tchat` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tchat`
--

INSERT INTO `tchat` (`id_tchat`, `date`, `id_partie`, `id_exp`, `id_dest`, `type`, `text_tchat`) VALUES
(748, '2018-09-21 13:51:19', 1, 2, 0, 1, 'salut'),
(749, '2018-09-21 13:51:31', 1, 2, 1, 1, 'ca va ?'),
(750, '2018-09-21 13:51:41', 1, 1, 0, 1, '8'),
(751, '2018-09-21 13:53:04', 1, 1, 2, 1, 'hahah'),
(752, '2018-09-21 13:53:20', 1, 2, 3, 1, 'TG'),
(753, '2018-09-21 13:54:39', 1, 2, 2, 2, 'vharung lance : /h 1d6 : 1'),
(829, '2018-11-26 09:20:47', 1, 1, 2, 1, 'ultimate : Ha!'),
(830, '2018-12-05 19:55:05', 1, 1, 0, 4, '48'),
(831, '2018-12-21 13:03:13', 1, 1, 0, 2, 'Alauch lance : /r 1d6+2 : 6'),
(832, '2018-12-21 13:04:56', 1, 1, 0, 4, '81'),
(833, '2018-12-21 13:05:02', 1, 1, 0, 4, '12'),
(834, '2018-12-21 13:05:17', 1, 1, 0, 2, 'Alauch lance : /r 1d4 : 3'),
(835, '2018-12-21 13:09:07', 1, 1, 2, 1, 'ultimate : bonjour'),
(836, '2018-12-21 13:28:42', 1, 1, 0, 4, '56'),
(837, '2018-12-21 16:11:21', 1, 1, 0, 4, '69'),
(838, '2018-12-22 09:08:43', 1, 1, 0, 2, 'Alauch lance : /r 1d6+2 : 6'),
(839, '2018-12-22 09:08:58', 1, 1, 0, 2, 'Alauch lance : /r 1d6+2 : 6'),
(840, '2018-12-22 09:09:08', 1, 1, 0, 2, 'Alauch lance : /r 1d6 : 3'),
(841, '2018-12-22 09:09:55', 1, 1, 0, 2, 'Alauch lance : /r 1d6+2 : 4'),
(842, '2018-12-22 09:10:14', 1, 1, 0, 2, 'Alauch lance : /r 1d6+2 : 3'),
(843, '2018-12-22 09:10:34', 1, 1, 0, 2, 'Alauch lance : /r 1d6+2 : 3'),
(844, '2018-12-22 09:10:54', 1, 1, 0, 2, 'Alauch lance : /r 1d6+2 : 6'),
(845, '2018-12-22 09:11:12', 1, 1, 0, 2, 'Alauch lance : /r 1d6+2 : 4'),
(846, '2018-12-22 09:14:24', 1, 1, 0, 2, 'Alauch lance : /r 1d6+2 : 1'),
(847, '2018-12-22 09:16:36', 1, 1, 0, 4, '72'),
(848, '2018-12-22 09:16:49', 1, 1, 0, 2, 'Alauch lance : /r 1d6+2 : 3'),
(849, '2018-12-22 09:17:06', 1, 1, 0, 4, '6'),
(850, '2018-12-22 09:17:21', 1, 1, 0, 6, '2'),
(851, '2018-12-22 09:17:32', 1, 1, 0, 4, '6'),
(852, '2018-12-22 09:19:42', 1, 1, 0, 2, 'Alauch lance : /r 1d6+2 : 1 : 51'),
(853, '2018-12-22 09:20:08', 1, 1, 0, 4, '60'),
(854, '2018-12-22 09:20:20', 1, 1, 0, 2, 'Alauch lance : /r 1d6+2 : 3 : 71'),
(855, '2018-12-22 09:24:14', 1, 1, 0, 2, 'Alauch lance : /r 1d6+2 : 2 : 24'),
(856, '2018-12-22 09:26:39', 1, 1, 0, 2, 'Alauch lance : /r 1d6+2 : 4 : 12'),
(857, '2018-12-22 11:06:38', 1, 1, 0, 2, 'Alauch lance : /r 1d6+2 : 4 : 49'),
(858, '2018-12-22 11:07:02', 1, 1, 0, 1, '/R 1d6+2'),
(859, '2018-12-22 11:07:13', 1, 1, 0, 2, 'Alauch lance : /r 1d6+2 : 5 : 6'),
(860, '2018-12-22 11:12:41', 1, 1, 0, 2, 'Alauch lance : /r 1d6 + 2 : 5'),
(861, '2018-12-22 11:13:07', 1, 1, 0, 2, 'Alauch lance : /r 1d6+2 : 6'),
(862, '2018-12-22 11:18:14', 1, 1, 0, 2, 'Alauch lance : /r 1d6+2 : 104'),
(863, '2018-12-22 11:19:10', 1, 1, 0, 2, 'Alauch lance : /r 1d6+2 : 52'),
(864, '2018-12-22 11:20:25', 1, 1, 0, 2, 'Alauch lance : /r 1d6+2 : 4'),
(865, '2018-12-22 12:46:22', 1, 1, 0, 2, 'Alauch utilise bouclier : /r 2d6+1 : 6');

-- --------------------------------------------------------

--
-- Structure de la table `template`
--

CREATE TABLE `template` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `template`
--

INSERT INTO `template` (`id`, `name`, `id_user`) VALUES
(1, 'Liber aventure', 0),
(3, 'Liber SF', 0);

-- --------------------------------------------------------

--
-- Structure de la table `template_block`
--

CREATE TABLE `template_block` (
  `id` int(11) NOT NULL,
  `id_template` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `block` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `valeur` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `template_block`
--

INSERT INTO `template_block` (`id`, `id_template`, `name`, `block`, `type`, `valeur`) VALUES
(1, 1, 'Point de destin', 1, 1, '3'),
(2, 1, 'module_des', 2, 2, '1d100 + 1d10|5|96|100|10|0'),
(3, 1, 'Module posture', 3, 3, NULL),
(6, 1, 'module_avatar', 4, 4, NULL),
(7, 1, 'module_bouclier', 4, 5, NULL),
(8, 1, 'module_bouclier_sec', 4, 6, NULL),
(9, 1, 'PV', 5, 7, '#4CAF50'),
(10, 1, 'PSY', 5, 7, '#2196F3'),
(11, 1, 'module_tchat', 6, 8, NULL),
(12, 1, 'module_note', 7, 9, 'Scénario|Partie précédente |Note'),
(13, 1, 'module_fiche', 8, 10, NULL),
(14, 1, 'Histoire', 10, 12, NULL),
(15, 1, 'Statistique', 11, 11, 'Phy|Soc|Men'),
(16, 1, 'Competence', 20, 13, NULL),
(17, 1, 'Carateristique', 21, 16, NULL),
(18, 1, 'Arme', 21, 14, NULL),
(19, 1, 'Encombrement', 30, 15, NULL),
(20, 1, 'Inventaire', 31, 14, NULL),
(21, 1, 'Connaissance', 32, 16, NULL),
(22, 1, 'Dons', 40, 16, NULL),
(23, 1, 'Posture', 41, 17, NULL),
(24, 1, 'Avantage/desavantage', 42, 18, NULL),
(25, 1, 'Etat', 43, 19, NULL),
(26, 1, 'Editeur', 44, 20, NULL),
(29, 1, 'Règle du jeu', 50, 50, 'Le MJ a toujours raison, TGCM<br>\r\nLe Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles, mais s\'est aussi adapté à la bureautique informatique, sans que son contenu n\'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.\r\n'),
(30, 1, 'module_doc', 51, 51, NULL),
(31, 1, 'module_pnj', 52, 52, NULL),
(32, 1, 'module_music', 53, 53, NULL),
(34, 1, '', 12, 21, 'For|Agi|Sag|Cha|Ast|Mem');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `mdp` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `name`, `mdp`, `email`) VALUES
(1, 'vharung', '9fcfea32bd28b16659d94bdc07f66a9b', 'contact@alexandre-crochet.fr'),
(2, 'ultimate', '9c8d9e1d80fc923ca7d532e46a3d606f', 'ulti@gmail.com'),
(3, 'test', '098f6bcd4621d373cade4e832627b4f6', 'test@test.fr');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `claque`
--
ALTER TABLE `claque`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `doc`
--
ALTER TABLE `doc`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `fiche_block`
--
ALTER TABLE `fiche_block`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `liste`
--
ALTER TABLE `liste`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `musique`
--
ALTER TABLE `musique`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `playlist`
--
ALTER TABLE `playlist`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `pnj`
--
ALTER TABLE `pnj`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tableau`
--
ALTER TABLE `tableau`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tables`
--
ALTER TABLE `tables`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tchat`
--
ALTER TABLE `tchat`
  ADD PRIMARY KEY (`id_tchat`);

--
-- Index pour la table `template`
--
ALTER TABLE `template`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `template_block`
--
ALTER TABLE `template_block`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `claque`
--
ALTER TABLE `claque`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `doc`
--
ALTER TABLE `doc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `fiche_block`
--
ALTER TABLE `fiche_block`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT pour la table `liste`
--
ALTER TABLE `liste`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `musique`
--
ALTER TABLE `musique`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `playlist`
--
ALTER TABLE `playlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT pour la table `pnj`
--
ALTER TABLE `pnj`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `tableau`
--
ALTER TABLE `tableau`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `tables`
--
ALTER TABLE `tables`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `tchat`
--
ALTER TABLE `tchat`
  MODIFY `id_tchat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=866;

--
-- AUTO_INCREMENT pour la table `template`
--
ALTER TABLE `template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `template_block`
--
ALTER TABLE `template_block`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
